#include <stdio.h>
#include <string.h>
#include <yajl/yajl_gen.h>
#include <yajl/yajl_version.h>
#include "i3status.h"

void print_mqtt_msg(yajl_gen json_gen, char *buffer, int index, const char *format, const char *topic, int max_length) {
    bool running = mqtt_is_connected();
    const char *walk;
    char *outwalk = buffer;

    START_COLOR((running ? "color_good" : "color_bad"));

        for (walk = format; *walk != '\0'; walk++) {
            if (*walk != '%') {
                *(outwalk++) = *walk;
                continue;
            }

            if (strncmp(walk+1, "topic", strlen("topic")) == 0) {
                outwalk += sprintf(outwalk, "%s", topic);
                walk += strlen("topic");
            } else if (strncmp(walk+1, "payload", strlen("payload")) == 0) {
                int msg_len;
                msg_len = snprintf(outwalk, (size_t)max_length, "%s", mqtt_get_last_message(index));
                if (msg_len >= max_length) {
                    outwalk += max_length-1;
                    outwalk += sprintf(outwalk, "...");
                } else {
                    outwalk += msg_len;
                }
                walk += strlen("payload");
            }
        }

    END_COLOR;

    OUTPUT_FULL_TEXT(buffer);
}
