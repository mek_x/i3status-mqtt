// vim:ts=4:expandtab

#include "i3status.h"

#include <stdio.h>
#include <string.h>
#include <mosquitto.h>
#include <unistd.h>
#include <error.h>

#define MAX_SUBSCRIPTIONS 10
#define MESSAGE_BUFFER 1024

static struct mosquitto* mqtt_client;
static volatile bool connected;


struct subscribed_topic
{
    bool registered;
    bool subscribed;
    const char * topic;
    char last_message[MESSAGE_BUFFER];
} subscriptions_list[MAX_SUBSCRIPTIONS];


static void subscribe_topic(int index)
{
    if (! connected)
        return;

    if (index < 0 || index >= MAX_SUBSCRIPTIONS)
        return;

    if (subscriptions_list[index].registered && (! subscriptions_list[index].subscribed))
    {
        mosquitto_subscribe(mqtt_client, NULL, subscriptions_list[index].topic, 0);
        subscriptions_list[index].subscribed = true;
    }
}

void connect_callback(struct mosquitto *m, void *data, int reason)
{
    if (0 == reason) {
        int i;

        connected = true;
        fprintf(stderr, "mqtt: connected\n");
        for (i = 0; i < MAX_SUBSCRIPTIONS; ++i)
            subscribe_topic(i);
    }
}

void disconnect_callback(struct mosquitto *m, void *data, int reason)
{
    if (0 != reason) {
        int i;

        connected = false;
        fprintf(stderr, "mqtt: disconnected\n");
        for (i = 0; i < MAX_SUBSCRIPTIONS; ++i)
        {
            subscriptions_list[i].subscribed = false;
        }
    }
}

void message_callback(struct mosquitto *m, void *data, const struct mosquitto_message *msg)
{
    int i;

    for (i = 0; i < MAX_SUBSCRIPTIONS; ++i)
        if (subscriptions_list[i].registered && (strstr(msg->topic, subscriptions_list[i].topic) != NULL))
            snprintf(subscriptions_list[i].last_message, 1024, "%s", (char *)msg->payload);
}

bool mqtt_is_connected(void)
{
    return connected;
}

void mqtt_loop(void)
{
    int ret = mosquitto_loop(mqtt_client, 100, 1000);
    if (0 != ret)
        mosquitto_reconnect(mqtt_client);
}

const char* mqtt_get_last_message(int index)
{
    if (index >= 0 && index < MAX_SUBSCRIPTIONS)
        return subscriptions_list[index].last_message;
    else
        return "";
}

void mqtt_register_subscription(int index, char * topic)
{
    if (index >= MAX_SUBSCRIPTIONS || index < 0)
    {
        fprintf(stderr, "mqtt: Bad subscription index");
        return;
    }

    if (subscriptions_list[index].registered)
        return;

    subscriptions_list[index].topic = topic;
    subscriptions_list[index].registered = true;

    subscribe_topic(index);
}

int mqtt_init(const char* host, int port, int keepalive, bool cleansession)
{
    int i;
    const char* client_id = "i3status";

    connected = false;
    mosquitto_lib_init();

    mqtt_client = mosquitto_new(client_id, cleansession, NULL);

    if(!mqtt_client) {
        perror("mqtt: mosquitto_new failed");
        return -1;
    }

    mosquitto_connect_callback_set(mqtt_client, connect_callback);
    mosquitto_disconnect_callback_set(mqtt_client, disconnect_callback);
    mosquitto_message_callback_set(mqtt_client, message_callback);

    int ret = mosquitto_connect(mqtt_client, host, port, keepalive);
    if (MOSQ_ERR_SUCCESS != ret) {
        fprintf(stderr, "mqtt: mosquitto_connect failed\n");
        return -1;
    }

    for (i = 0; i < MAX_SUBSCRIPTIONS; ++i)
    {
        subscriptions_list[i].registered = false;
        subscriptions_list[i].subscribed = false;
    }

    return 0;
}

void mqtt_destroy(void)
{
    if(mqtt_client) {
        mosquitto_disconnect(mqtt_client);
        mosquitto_destroy(mqtt_client);
    }
    mosquitto_lib_cleanup();
}
