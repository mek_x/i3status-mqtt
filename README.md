i3status-mqtt
=============

Added simple mqtt subscription plugin.

Made as a practical programming exercise and for fun :).


Config file example:
```
general {
        colors = true
        interval = 1
        mqtt_host = localhost
        mqtt_port = 1883
        mqtt_keepalive = 60
        mqtt_cleansession = true
}

order += "mqtt 0"
order += "mqtt 1"
order += "mqtt 2"
order += "disk /"
order += "run_watch DHCP"
order += "run_watch VPN"
order += "wireless wlan0"
order += "ethernet eth0"
order += "battery 0"
order += "load"
order += "tztime local"

mqtt 0 {
        format = "%topic: %payload"
        topic = "/example/0"
        max_length = 10
}

mqtt 1 {
        format = "%topic: %payload"
        topic = "/example/1"
        max_length = 60
}

mqtt 2 {
        format = "%topic: %payload"
        topic = "/example/2"
}

wireless wlan0 {
        format_up = "W: (%quality at %essid) %ip"
        format_down = "W: down"
}

ethernet eth0 {
        # if you use %speed, i3status requires root privileges
        format_up = "E: %ip (%speed)"
        format_down = "E: down"
}

battery 0 {
        format = "%status %percentage %remaining"
}

run_watch DHCP {
        pidfile = "/var/run/dhclient*.pid"
}

run_watch VPN {
        pidfile = "/var/run/vpnc/pid"
}

tztime local {
        format = "%Y-%m-%d %H:%M:%S"
}

load {
        format = "%1min"
}

disk "/" {
        format = "%free"
}
```

As a title, the mqtt section takes an integer in range from 0 to 9. This is
an identification of subscription entry. We may have up to 10 subscriptions.
